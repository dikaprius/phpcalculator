<?php

namespace phpcalculator\src\Helper;

use Carbon\Carbon;

class MyHelper
{
    /**
     * @var string
     */
    private $operatorString;

    /**
     * @var string
     */
    private $sign;

    const DIR = __DIR__ . "/histories.text";

    /**
     * MyHelper constructor.
     * @param string $sign
     */
    public function __construct($sign = '')
    {
        $this->sign = $sign;
        switch ($this->sign) {
            case 'add':
                $this->operatorString = ' + ';
                break;
            case 'subtract':
                $this->operatorString = ' - ';
                break;
            case 'multiply':
                $this->operatorString = ' * ';
                break;
            case 'divide':
                $this->operatorString = ' / ';
                break;
            case 'pow':
                $this->operatorString = ' ^ ';
                break;
            default:
                $this->operatorString = ' + ';
                break;
        }
    }

    /**
     * Collect the numbers with operator
     *
     * @param array $numbers
     * @return string
     */
    public function collectNumbers(array $numbers): string
    {
        return implode($this->operatorString, $numbers);
    }

    /**
     * calculate all the collected numbers
     *
     * @param array $numbers
     * @return float|int|mixed
     */
    public function calculateNumbers(array $numbers)
    {
        $number = array_pop($numbers);

        // If user input only 1 number then the result will be this number
        // Because there's no need to count with.
        if (count($numbers) <= 0) {
            return $number;
        }

        return $this->calculate($this->calculateNumbers($numbers), $number);
    }

    /**
     * calculate number based on sign
     *
     * @param $number1
     * @param $number2
     * @return float|int
     */
    public function calculate($number1, $number2)
    {
        switch ($this->sign) {
            case 'add':
                return $number1 + $number2;
            case 'subtract':
                return $number1 - $number2;
            case 'multiply':
                return $number1 * $number2;
            case 'divide':
                return $number1 / $number2;
            case 'pow':
                return $number1 ** $number2;
        }
    }

    /**
     * Store histories into histories.text
     *
     * @param $data
     * @param $collect
     * @param $result
     * @param int $flag
     * @return bool|int
     */
    public function histories($data,$collect, $result, $flag = 0)
    {
        return file_put_contents(self::DIR, '1' .','.$this->sign.','.$collect.','.$result.','.$data. ','. Carbon::now(). "|", FILE_APPEND | LOCK_EX);
    }
}
