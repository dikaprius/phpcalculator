<?php

namespace phpcalculator\src\Commands;

use Illuminate\Console\Command;
use phpcalculator\src\Helper\MyHelper;

class Subtract extends Command
{

    /**
     * @var string
     */
    protected $signature = 'subtract {numbers* : the numbers to be subtract}';

    /**
     * @var string
     */
    protected $description = "Subtract the numbers";


    /**
     * Handler
     * Execute the calculation
     */
    public function handle(): void
    {
        $helper = new MyHelper('subtract');

        $numbers = $this->number(); // Numbers to be calculate
        $collect = $helper->collectNumbers($numbers); // collect all numbers with the operator
        $result = $helper->calculateNumbers($numbers); // Calculate the numbers

        $data = sprintf('%s = %s', $collect, $result);

        // Store to history
        $helper->histories($data, $collect, $result);

        // Print the data
        $this->comment($data);
    }

    /**
     * get argument type
     *
     * @return array
     */
    protected function number(): array
    {
        return $this->argument('numbers');
    }
}
