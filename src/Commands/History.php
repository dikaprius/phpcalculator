<?php

namespace phpcalculator\src\Commands;

use Illuminate\Console\Command;
use phpcalculator\src\Helper\MyHelper;
use Symfony\Component\Console\Helper\Table;

class History extends Command
{
    /**
     * @var string
     */
    protected $signature = "history:list {--this_argument* : This Description}";

    protected $name = 'history';

    /**.
     * @var string
     */
    protected $description = "show histories";

    /**
     * Handler
     */
    public function handle(): void
    {
        if ($this->hasArgument('this_argument')){
            $arguments = $this->text();

            $filename = MyHelper::DIR;
            // Retrieve all the contents in histories.txt file
            $fileHistories = file_get_contents($filename);
            $data = explode('|', $fileHistories);

            //If line in histories.text < 1 then print "History is empty !"
            if (strlen($fileHistories) < 1) {
                $this->comment('History is empty !');
                return;
            }

            $rows = array();

            // Create table for histories
            $table = new Table($this->output);
            $table->setHeaders(['No. ', 'Command', 'Description', 'Result', 'Output', 'Time']);
            foreach ($data as $k => $value) {
                $rows[] = explode(',', $value);
                $rows[$k][0] = $k + 1;

                // 6 is for amount of headers
                // to delete the last line
                if (sizeof($rows[$k]) < 6) {
                    unset($rows[$k]);
                }

                if (sizeof($arguments) > 0) {
                    if (!in_array($rows[$k][1], $arguments)) {
                        unset($rows[$k]);
                    }
                }

            }

            $table->setRows($rows);

            $table->render();
        }else {

            $filename = MyHelper::DIR;
            // Retrieve all the contents in histories.txt file
            $fileHistories = file_get_contents($filename);
            $data = explode('|', $fileHistories);

            //If line in histories.text < 1 then print "History is empty !"
            if (strlen($fileHistories) < 1) {
                $this->comment('History is empty !');
                return;
            }

            $rows = array();

            // Create table for histories
            $table = new Table($this->output);
            $table->setHeaders(['No. ', 'Command', 'Description', 'Result', 'Output', 'Time']);
            foreach ($data as $k => $value) {
                $rows[] = explode(',', $value);
                $rows[$k][0] = $k + 1;

                // 6 is for amount of headers
                // to delete the last line
                if (sizeof($rows[$k]) < 6) {
                    unset($rows[$k]);
                }

            }

            $table->setRows($rows);

            $table->render();
        }

    }


    /**
     * get argument type
     *
     * @return array
     */
    protected function text(): array
    {
        return $this->argument('this_argument');
    }
}

