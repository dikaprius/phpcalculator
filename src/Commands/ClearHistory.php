<?php

namespace phpcalculator\src\Commands;

use Illuminate\Console\Command;
use phpcalculator\src\Helper\MyHelper;

class ClearHistory extends Command
{
    /**
     * @var string
     */
    protected $signature = 'history:clear';

    protected $name = 'history';

    /**.
     * @var string
     */
    protected $description = "clear histories";

    /**
     * Handler
     */
    public function handle(): void
    {
        // rewrite/update histories.text file with empty string to clear it up
        file_put_contents(MyHelper::DIR, '');

        // Print out "History cleared!"
        $this->comment("History cleared!");
    }
}
