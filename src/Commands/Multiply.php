<?php

namespace phpcalculator\src\Commands;

use Illuminate\Console\Command;
use phpcalculator\src\Helper\MyHelper;

class Multiply extends Command
{
    /**
     * @var string
     */
    protected $signature = 'multiply {numbers* : The numbers to be multiply}';

    /**.
     * @var string
     */
    protected $description = "multiply the numbers";


    /**
     * Handler
     * Execute the calculation
     */
    public function handle(): void
    {
        $helper = new MyHelper('multiply');

        $numbers = $this->number(); // Numbers to be calculate
        $collect = $helper->collectNumbers($numbers); // collect all numbers with the operator
        $result = $helper->calculateNumbers($numbers); // Calculate the numbers

        $data = sprintf('%s = %s', $collect, $result);

        // Store to history
        $helper->histories($data, $collect, $result);

        // Print the data
        $this->comment($data);
    }

    /**
     * get argument type
     *
     * @return array
     */
    protected function number(): array
    {
        return $this->argument('numbers');
    }
}
