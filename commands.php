<?php

return [
    // TODO : Add list of commands here
    \phpcalculator\src\Commands\Add::class,
    \phpcalculator\src\Commands\Subtract::class,
    \phpcalculator\src\Commands\Divide::class,
    \phpcalculator\src\Commands\Multiply::class,
    \phpcalculator\src\Commands\Pow::class,
    \phpcalculator\src\Commands\History::class,
    \phpcalculator\src\Commands\ClearHistory::class,
];
